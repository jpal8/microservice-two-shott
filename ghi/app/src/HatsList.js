import React from 'react'

async function Delete(id) {
    const url = "http://localhost:8090/api/hats/" + id;
    const fetchConfig = { method: "DELETE" };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
        window.location.reload(false);
    }
}

class HatsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hats: []
        };
    }

    async componentDidMount() {
        const url = "http://localhost:8090/api/hats";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ hats: data.hats });
            console.log(data.hats)
        }
    }

    render() {
        return (
            <div>
                <h1>Hats</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Fabric</th>
                            <th>Style</th>
                            <th>Color</th>
                            <th>Image</th>
                            <th>Delete</th>

                        </tr>
                    </thead>
                    <tbody>
                        {this.state.hats.map(hat => {
                            return (
                                <tr key={hat.href}>
                                    <td>{hat.fabric}</td>
                                    <td>{hat.style_name}</td>
                                    <td>{hat.color}</td>
                                    <td><img src={hat.picture_url} alt="" width="100" height="100" /></td>
                                    <td><button onClick={() => Delete(hat.id)}>Delete</button></td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
                <button><a href="http://localhost:3000/hats/new/">Create a hat</a></button>
            </div>
        )
    }
}

export default HatsList;
