import React, { useEffect, useState } from 'react'

function ShoeForm() {

    const [bins, setBins] = useState([])

    const [manufacturer, setManufacturer] = useState('')
    const handleChangeManufacturer = (event) => {
        const value = event.target.value
        setManufacturer(value)
    }

    const [modelName, setModelName] = useState('')
    const handleChangeModelName = (event) => {
        const value = event.target.value
        setModelName(value)
    }


    const [color, setColor] = useState('')
    const handleChangeModelColor = (event) => {
        const value = event.target.value
        setColor(value)
    }

    const [pictureUrl, setPictureUrl] = useState('')
    const handleChangePictureUrl = (event) => {
        const value = event.target.value
        setPictureUrl(value)

    }

    const [bin, setBin] = useState('')
    const handleChangeBin = (event) => {
        const value = event.target.value
        setBin(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}
        data.model_name = modelName
        data.manufacturer = manufacturer
        data.color = color
        data.picture_url = pictureUrl
        data.bin = bin

        const shoeUrl = 'http://localhost:8080/api/shoes/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(shoeUrl, fetchConfig)
        if (response.ok) {
            const shoe = await response.json()

            setManufacturer('')
            setModelName('')
            setColor('')
            setPictureUrl('')
            setBin('')
        }

    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/'

        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setBins(data.bins)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a New Shoe</h1>
                    <form onSubmit={handleSubmit} id="create-shoes-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleChangeManufacturer} required placeholder="Manufacturer" type="text" id="manufacturer" name="manufacturer" className="form-control" value={manufacturer} />
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChangeModelName} required placeholder="Model Name" type="text" id="model_name" name="model_name" className="form-control" value={modelName} />
                            <label htmlFor="model_name">Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChangeModelColor} required placeholder="Model Color" type="text" id="color" name="color" className="form-control" value={color} />
                            <label htmlFor="color">Model Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChangePictureUrl} required placeholder="Picture URL" type="href" id="picture_url" name="picture_url" className="form-control" value={pictureUrl} />
                            <label htmlFor="picture_url">Picture URL</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleChangeBin} required placeholder="bins" id="bin" className="form-select" value={bin}>
                                <option value="">Select a bin</option>
                                {bins.map(bin => {
                                    return (
                                        <option key={bin.id} value={bin.href}>
                                            {bin.bin_number}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className='btn btn-primary'>Create</button>
                    </form>
                </div>
            </div>

        </div>
    )
}

export default ShoeForm
