import React, { useEffect, useState } from 'react'

function ShoesList() {

    const [shoes, setShoes] = useState([])
    const getData = async () => {
        const resp = await fetch('http://localhost:8080/api/shoes')
        if (resp.ok) {
            const data = await resp.json()
            setShoes(data.shoes)
        }
    }

    const Delete = async (id) => {
        const resp = await fetch(`http://localhost:8080/api/shoes/${id}`, {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json',
            },
        })
        if (resp.ok) {
            setShoes(shoes.filter(shoe => shoe.id !== id))
        }
    }
    useEffect(() => {
        getData()
    }, [])

    return (
        <div>
            <div className='mb-3 mt-3'>
                <button><a style={{ textDecoration: 'none' }} href="http://localhost:3000/shoes/new/">Create a Shoe</a></button>
            </div>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th></th>
                        <th>Manufacturer</th>
                        <th>Shoe Model</th>
                        <th>Shoe Color</th>
                        <th>Bin</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {shoes.map(shoe => {
                        return (
                            <tr key={shoe.href}>
                                <td>
                                    <img src={shoe.picture_url} alt="shoe" width="50" height="50" />
                                </td>
                                <td>{shoe.manufacturer}</td>
                                <td>{shoe.model_name}</td>
                                <td>{shoe.color}</td>
                                <td>{shoe.bin.bin_number}</td>
                                <td><button className="btn btn-secondary" onClick={() => Delete(shoe.id)}>Delete</button></td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default ShoesList
