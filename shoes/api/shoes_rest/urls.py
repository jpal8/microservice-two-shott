from django.urls import path
from .views import (
    shoes_list,
    shoes_detail,
    
)

urlpatterns = [
    path("shoes/", shoes_list, name="shoes_list"),
    path("shoes/<int:pk>/", shoes_detail, name="shoes_list")

]
