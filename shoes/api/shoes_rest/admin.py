from django.contrib import admin

# Register your models here.
from .models import Shoes, BinVO


@admin.register(Shoes)
class ShoesAdmin(admin.ModelAdmin):
    pass


@admin.register(BinVO)
class BinVOAdmin(admin.ModelAdmin):
    pass
