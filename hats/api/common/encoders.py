from hats_rest.models import LocationVO, Hats
from common.json import ModelEncoder


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href", "closet_name"]


class LocationVODetailsEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href", "closet_name", "section_number", "shelf_number"]


class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = ["fabric", "style_name", "color", "picture_url", "location"]

    encoders = {"location": LocationVODetailsEncoder()}


class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]

    encoders = {"location": LocationVODetailsEncoder()}
