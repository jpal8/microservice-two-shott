from django.db import models
from django.urls import reverse


# Create your models here.
class LocationVO(models.Model):
    import_href = models.CharField(max_length=100, unique=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveIntegerField()
    shelf_number = models.PositiveIntegerField()


class Hats(models.Model):
    fabric = models.CharField(max_length=100)
    style_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(blank=True, null=True)
    location = models.ForeignKey(
        LocationVO, related_name="hats", on_delete=models.CASCADE
    )

    def get_api_url(self):
        return reverse("hats.detail", kwargs={"pk": self.pk})
